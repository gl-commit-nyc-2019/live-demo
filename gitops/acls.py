import yaml

class ACLS:
    def __init__(self, file):
        with open(file) as f:
            self.yaml = yaml.load(f, Loader=yaml.FullLoader)

        self.name = self.yaml['Group']
        self.roles = self.yaml['Roles']