import gitlab
import yaml

ROLES = {
    "owner": gitlab.OWNER_ACCESS,
    "maintainer": gitlab.MAINTAINER_ACCESS,
    "developer": gitlab.DEVELOPER_ACCESS,
    "reporter": gitlab.REPORTER_ACCESS,
    "guest": gitlab.GUEST_ACCESS
}

class Resource:
    def __init__(self, file):
        with open(file) as f:
            self.yaml = yaml.load(f, Loader=yaml.FullLoader)

        self.name = self.yaml['Role']
        self.role = ROLES[self.name]

        if 'Users' in self.yaml:
            self.users = self.yaml['Users']