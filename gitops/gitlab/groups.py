import gitlab

from .users import User

class Group:
    def __init__(self, client, name):
        self.client = client
        self.group = self.client.getGroup(name)

    def isMember(self, user):
        try:
            u = User(self.client, user)
            if not u.user:
                return None
            self.group.members.get(u.user.id)
            return True
        except:
            return False

    def accessLevel(self, role, user):
        try:
            u = User(self.client, user)
            if not u.user:
                return None
            member = self.group.members.get(u.user.id)
            if member.access_level == role:
                return True
        except:
            return False

    def getAllUsers(self):
        try:
            return self.group.members.list()
        except:
            return []

    def addUser(self, role, user):
        try:
            if not self.isMember(user):
                u = User(self.client, user)
                if not u.user:
                    return None

                self.group.members.create({'user_id': u.user.id, 'access_level': role})
                print("PASS: Add user:{u} to group:{g} with role:{r}".format(u=user,
                      g=self.group.name, r=role))
                return True
        except Exception as e:
            print(e)
            return False

        
    def updateUser(self, role, user):
        try:
            if self.isMember(user):
                if not self.accessLevel(role, user):
                    u = User(self.client, user)
                    if not u.user:
                        return None
                    member = self.group.members.get(u.user.id)
                    member.access_level = role
                    member.save()
                    print("PASS: Update user:{u} in group:{g} with role:{r}".format(u=user,
                          g=self.group.name, r=role))
            return True
        except Exception as e:
            print("E: ", e)
            return False


    def deleteUser(self, user):
        try:
            if self.isMember(user):
                u = User(self.client, user)
                if not u.user:
                    return None

                if self.accessLevel(gitlab.OWNER_ACCESS, user):
                    return False

                self.group.members.delete(u.user.id)
                print("PASS: Delete user:{u} from group:{g}".format(u=user,
                      g=self.group.name))
            return True 
        except:
            return False

        