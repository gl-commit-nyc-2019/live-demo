import gitlab

class Client:
    def __init__(self, url, token):
        self.gl = gitlab.Gitlab(url, private_token=token)

    def getGroup(self, group):
        return self.gl.groups.get(group)

    def getUser(self, user):
        try:
            u = self.gl.users.list(username=user)[0]
            return u
        except:
            return None
