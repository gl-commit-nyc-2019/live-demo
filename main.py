import gitlab
import os
import yaml

from gitops.gitlab.client import Client
from gitops.gitlab.groups import Group
from gitops.gitlab.users import User
from gitops.acls import ACLS
from gitops.resources import Resource

GITLAB = "https://gitlab.com"
TOKEN = os.getenv("GL_TOKEN")

    

if __name__ == "__main__":
    a = ACLS('acls.yaml')

    c = Client(GITLAB, TOKEN)
    g = Group(c, 'gl-commit-nyc-2019')

    for role in a.roles:
        r = Resource('{r}.yaml'.format(r=role))

        if hasattr(r, 'users'):
            for user in r.users:
                if g.isMember(user):
                    g.updateUser(r.role, user)
                else:
                    g.addUser(r.role, user)

    skip = []
    for role in a.roles:
        r = Resource('{r}.yaml'.format(r=role))
        if hasattr(r, 'users'):
            for user in r.users:
                skip.append(user)

    for delete in g.getAllUsers():
        if delete.username not in skip:
            g.deleteUser(delete.username)
